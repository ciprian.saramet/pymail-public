# no shebang if you're on Windows :)

import sys
import glob
import os
import json
import csv
import msal
import logging
import requests
import chilkat as chilkat
import tabula as tab
import pandas as pd
import pprint
import base64
import mimetypes
import os
import pprint
import uuid

fisiere = []
ceseveuri= []
jsons = []

def get_token(): # we start by getting a token
    # Optional logging
    # logging.basicConfig(level=logging.DEBUG)  # Enable DEBUG log for entire script
    # logging.getLogger("msal").setLevel(logging.INFO)  # Optionally disable MSAL DEBUG logs

    os.chdir('working_dir_path') # this is where we store our private.key, parameters.json and our python program
    with open("parameters.json", "r") as json_file: # setup example below --> you need details from your tennant
        config = json.load(json_file)
        # This can either be set up with a secret or a certificate.This example uses a certificate    
        #{
        #"authority": "https://login.microsoftonline.com/tennant_name",
        #"client_id": "app client id from Azure",
        #"scope": ["https://graph.microsoft.com/.default"],
        #"thumbprint": "enter cert thumbprint here",
        #"private_key_file": "",
        #"endpoint": "https://graph.microsoft.com/v1.0/users"
        #}

    # Create a preferably long-lived app instance that maintains a token cache.
    app = msal.ConfidentialClientApplication(
        config["client_id"], authority=config["authority"],
        client_credential={"thumbprint": config["thumbprint"], "private_key": open(config['private_key_file']).read()},
        # token_cache=...  # Default cache is in memory only.
                           # You can learn how to use SerializableTokenCache from
                           # https://msal-python.rtfd.io/en/latest/#msal.SerializableTokenCache
        )

    # The pattern to acquire a token looks like this.
    result = None

    # First, the code looks up a token from the cache.
    # Because we're looking for a token for the current app, not for a user,
    # use None for the account parameter.
    result = app.acquire_token_silent(config["scope"], account=None)
    if not result:
        logging.info("No suitable token exists in cache. Let's get a new one from AAD.")
        result = app.acquire_token_for_client(scopes=config["scope"])

    if "access_token" in result:
        # Call a protected API with the access token.
        print('A successful ' + result["token_type"] + ' token obtained!')
    else:
        print(result.get("error"))
        print(result.get("error_description"))
        print(result.get("correlation_id"))  # You might need this when reporting a bug.
    return result    

def save_emails_to_json(): # with a valid token we get a list of emails from an endpoint search string
    result = get_token()
    endpoint = 'https://graph.microsoft.com/v1.0/users/{user_id}}/mailFolders/Inbox/messages?$search="hasAttachments=true"'

    params = {'hasAttachments': 'True'}
    http_headers = {'Authorization': 'Bearer ' + result['access_token'],
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                    }
    data = requests.get(endpoint, headers=http_headers, params=params, stream=False).json()
    # save query result to json file --> emails with attachments
    with open('emails.json', 'w') as jsonfile:
        json.dump(data, jsonfile)

def load_messages(): # parsing through the emails saved in a local json to find emails with attachments

    with open('emails.json', 'r') as jsonfile:
        messages = []
        data = json.load(jsonfile)
        for id in data['value']:
            messages.append(id)
        return messages

def get_mail_idz(): # gets the email ID's of emails with attachments
    emailidz = []
    emails = load_messages()
    for email in emails:
        emailidz.append(email['id'])
    return emailidz   

def chilkat_unlock(): # unleashes the power of Chilkat
    # The Chilkat API can be unlocked for a fully-functional 30-day trial by passing any
    # string to the UnlockBundle method.  A program can unlock once at the start. Once unlocked,
    # all subsequently instantiated objects are created in the unlocked state. 
    # 
    # After licensing Chilkat, replace the "Anything for 30-day trial" with the purchased unlock code.
    # To verify the purchased unlock code was recognized, examine the contents of the LastErrorText
    # property after unlocking.  For example:
    glb = chilkat.CkGlobal()
    success = glb.UnlockBundle("Anything for 30-day trial")
    if (success != True):
        print(glb.lastErrorText())
        sys.exit()

    status = glb.get_UnlockStatus()
    if (status == 2):
        print("Unlocked using purchased unlock code.")
    else:
        print("Unlocked in trial mode.")

    # The LastErrorText can be examined in the success case to see if it was unlocked in
    # trial more, or with a purchased unlock code.
    print(glb.lastErrorText())

def get_attachments(): # iterative function to pull attachments for message id's obtained in get_email_idz()
    # http.SetUrlVar needs a UPN set up
    token = get_token()
    http = chilkat.CkHttp()
    # we first get the token to authenticate to MS Graph
    token_id = token['access_token']
    # then we use the token to issue chilkat requests
    http.put_AuthToken(token_id)
    # Sends:  GET /users/{user_id | userPrincipalName}/messages/{message_id}/attachments
    # Note: It is also possible to use the literal string "me" for the current logged-on user.
    # For example: GET /me/messages/{message_id}/attachments
    sbResponse = chilkat.CkStringBuilder()
    http.ClearUrlVars()
    http.SetUrlVar("userPrincipalName","user@domain.com") # this is where your mail-enabled user goes
    #we've gathered the email id's into the function below
    message_ids = get_mail_idz()
    # Send the request to download the attachments for each of the emails
    for messageid in message_ids:
        http.SetUrlVar("message_id", messageid)
        print('set mess id')
        success = http.QuickGetSb("https://graph.microsoft.com/v1.0/users/{$userPrincipalName}/messages/{$message_id}/attachments", sbResponse)
        if ((success != True) and (http.get_LastStatus() == 0)):
            print(http.lastErrorText())
            print('fail')
            sys.exit()

        # The attachment data is contained within the JSON response.
        json = chilkat.CkJsonObject()
        json.LoadSb(sbResponse)
        json.put_EmitCompact(False)
        print("Status code = " + str(http.get_LastStatus()))
        if (http.get_LastStatus() != 200):
            print(json.emit())
            print("Failed.")
        sbSavePath = chilkat.CkStringBuilder()
        attachData = chilkat.CkBinData()
        lastMod = chilkat.CkDateTime()
        fac = chilkat.CkFileAccess()

        i = 0
        numMessages = json.SizeOfArray("value")
        while i < numMessages :
            json.put_I(i)

            print("name: " + json.stringOf("value[i].name"))
            print("contentType: " + json.stringOf("value[i].contentType"))
            sizeInBytes = json.IntOf("value[i].size")
            print("size: " + str(sizeInBytes))

            # Extract the data and save to a file.
            sbSavePath.SetString("working_dir") # change to your working directory
            sbSavePath.Append(json.stringOf("value[i].name"))

            attachData.Clear()
            attachData.AppendEncoded(json.stringOf("value[i].contentBytes"),"base64")
            attachData.WriteFile(sbSavePath.getAsString())

            # Get the last-modified date/time and set the output file's last-mod date/time..
            lastMod.SetFromTimestamp(json.stringOf("value[i].lastModifiedDateTime"))
            fac.SetLastModified(sbSavePath.getAsString(),lastMod)

            print("----")

            i = i + 1

def get_pdfs(): # iterates through attachments and pulls PDF's in a list
    os.chdir("working_dir") # you should know what this is by now.
    for file in glob.glob("*.pdf"):
        fisiere.append(os.path.join("working_dir",file))
    return fisiere

def converts_pdfs_to_csvs():
    pdfs = get_pdfs()
    for fisier in pdfs:
        nume_fisier, extensie_fisier = os.path.splitext(fisier) # this splits filename and extension
        tab.convert_into(fisier, nume_fisier +'.csv', output_format="csv", pages='all') # this saves the csv with filename as title

def get_csvs(): # iterates through attachments and pulls PDF's converted to CSV's in another list
    for file in glob.glob("*.csv"):
        ceseveuri.append(os.path.join("working_dir",file))
    return ceseveuri

def converts_csvs_to_json():
    csvs = get_csvs()
    for fisier in csvs:
        nume_fisier, extensie_fisier = os.path.splitext(fisier)
        with open(fisier) as csv_file:
            csvreader = csv.DictReader(csv_file, fieldnames = ('Client', 'Matter', 'Document Name', 'Number', 'Version', 'Database', 'User', 'Activity\nCount', 'DATETIME'))
            # not an elegant way to build the json keys = column names = fieldnames from the csv files
            out = json.dumps( [row for row in csvreader ] ) # every row in the csv gets dumped by the dict comprehension and saved to json files
            json_file = open(nume_fisier+'.json','w')
            json_file.write(out)

def send_email(file,report): # look at load_jsons() first
    result = get_token()
    endpoint = 'https://graph.microsoft.com/v1.0/users/{user_id}}/sendMail'

    params = {
	"Message": {
		"toRecipients": [{
			"emailAddress": {
                "address": "john.doe@covid19.com", # tribute to the pandemic
            }

		}],
		"Body": {
			"Content": json.dumps(report) # this is the tricky bit when embedding .json into an email body
		},
		"Subject": file
	},
	"SaveToSentItems": "true"
}
    data = json.dumps(params)
    pprint.pprint(data)        

    http_headers = {'Authorization': 'Bearer ' + result['access_token'],
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                    }
    req = requests.post(endpoint, data=data, headers=http_headers, stream=True)
    print(req)               

def load_jsons(): # loads all json files saved on disk by converts_csvs_to_json() in a list
    os.chdir('working_dir')
    mydir = 'working_dir' # i hate code duplication...
    for file in os.listdir(mydir):
        if file.endswith(".json"):
            fisier = os.path.join(mydir,file)
            fisiere.append(fisier)
            print(fisiere)
    while fisiere: # while the list has items in it load each json file and send it by email
        jason = fisiere.pop()
        try:
            with open(jason, 'r', encoding='utf-8') as json_file:
                json_report = json.loads(json_file.read())
                report = json_report[1::]
                pprint.pprint(report)
                jason = os.path.split(jason)
                nume_fisier, extensie_fisier = os.path.splitext(jason[1])
                send_email(nume_fisier,report)
        except UnicodeDecodeError: # because people put all sorts of characters in those damn pdf's
            continue        

def cleanup(): # cleans all the crap off our disk getting it ready for the next batch
    os.chdir('working_dir')
    types = ('*.csv','*.pdf','*.json')
    files_grabbed = []
    for files in types:
        files_grabbed.extend(glob.glob(files)) # God bless stackOverflow for this one

    for f in files_grabbed:
        print(f)
        try:
            os.remove(f)
        except OSError as e:
            print("Error: %s : %s" % (f, e.strerror))

def __main__(): # this is where the magic happens
    os.chdir('working_dir')
    chilkat_unlock()
    save_emails_to_json()
    get_attachments()
    get_pdfs()
    converts_pdfs_to_csvs()
    get_csvs()
    converts_csvs_to_json()
    load_jsons()
    cleanup()


if __name__ == "__main__":
    __main__()