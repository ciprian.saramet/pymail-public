**PyMail**

*author and maintainer: Ciprian Saramet - cipriansaramet.ro*

PyMail is an app that helps you automate various tasks by leveraging a 
connection to MS Graph.

I used this to send some email alerts in json format to Azure Sentinel.
The tricky part is that i had to pull the info in the json format from some
pdf files that were being sent by Prosperoware Milan to an email address.

This script queries a cloud-enabled mailbox, pulls PDF attachments in a folder
and then converts them to CSV files using a java wrapper.

It then takes those CSV's and converts them to json files and after that it 
appends the individual reports in json format to emails and sends them to a 
designated address.

As soon as all emails are sent the script then cleans up the temp location where
it stored the attachments.
This is designed to run as a scheduled task in production.

For using Pymail you need to register your app in the Azure tennant in order 
to acquire a token.

You then need to visit Application Permissions in the portal and declare 
scope User.Read.All, which needs admin consent.

Read more about it here:
https://bit.ly/33KFZnJ
https://bit.ly/3axkOb2
https://bit.ly/3am0mtJ

We used a chilkat library for part of the code.Java is a pre-requisite for the
chilkat library to work.
